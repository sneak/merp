module github.com/sneak/merp

go 1.13

require (
	github.com/astaxie/beego v1.12.0
	github.com/didip/tollbooth v4.0.2+incompatible
	github.com/didip/tollbooth_gin v0.0.0-20170928041415-5752492be505
	github.com/dn365/gin-zerolog v0.0.0-20171227063204-b43714b00db1
	github.com/fatih/structtag v1.1.0 // indirect
	github.com/gin-gonic/gin v1.4.1-0.20191101024740-db9174ae0c25
	github.com/google/uuid v1.1.1
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-runewidth v0.0.6 // indirect
	github.com/mgechev/dots v0.0.0-20190921121421-c36f7dcfbb81 // indirect
	github.com/mgechev/revive v0.0.0-20191017201419-88015ccf8e97 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/olekukonko/tablewriter v0.0.2 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/rs/zerolog v1.16.0
	github.com/thoas/stats v0.0.0-20190407194641-965cb2de1678
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a
	golang.org/x/sys v0.0.0-20191110163157-d32e6e3b99c4 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	golang.org/x/tools v0.0.0-20191107185733-c07e1c6ef61c // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
