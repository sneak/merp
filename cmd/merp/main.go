package main

import "os"
import "sync"
import "time"

import "github.com/rs/zerolog"
import "github.com/rs/zerolog/log"
import "github.com/sneak/merp"
import "golang.org/x/crypto/ssh/terminal"

//revive:disable
var Version string
var Buildtime string
var Builduser string
var Buildarch string
var Appname string

//revive:enable

func main() {
	initLogging()
	identify()
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		ms := merp.NewServer()
		ms.ServeForever()
		wg.Done()
	}()

	wg.Wait()
}

func identify() {
	log.Info().
		Str("app", Appname).
		Str("version", Version).
		Str("buildarch", Buildarch).
		Str("buildtime", Buildtime).
		Str("builduser", Builduser).
		Msg("starting")
}

func initLogging() {

	// always log in UTC
	zerolog.TimestampFunc = func() time.Time {
		return time.Now().UTC()
	}

	log.Logger = log.With().Caller().Stack().Logger()

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano}
		log.Logger = zerolog.New(output).With().Caller().Stack().Logger().With().Timestamp().Logger()
	}

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("DEBUG") != "" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
}
