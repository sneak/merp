# merp

clone of a popular iot api to learn more go

# Features

* send merps
* get latest merp
* get latest merps

# TODO

* merp notifications to long polling clients
* merp notifications via email
* pruning of old merps
* sending of merps using POST instead of querystring GET

# notes

the source code from this repo and all deps are included in the Docker image
by the Dockerfile, vendoring them into the build artifact without requiring
that they be copied into the git repo.

# author

sneak [sneak@sneak.berlin](mailto:sneak@sneak.berlin)
