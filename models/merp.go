package models

import "time"

// Merp is the data model for storing merps in the db
type Merp struct {
	ID       int       `orm:"auto;column(id)"`
	Content  string    `json:"content" orm:"type(jsonb);column(content)"`
	Created  time.Time `orm:"auto_now_add;type(datetime);column(created)"`
	RemoteIP string    `orm:"size(128);column(remoteip)"`
	Thing    string    `json:"thing" orm:"index;size(256)"`
	UUID     string    `orm:"size(36);column(uuid)"`
}
