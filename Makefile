APPNAME := merp

VERSION := $(shell git rev-parse HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)
BUILDTIMETAG := $(shell date -u '+%Y%m%d%H%M%S')
BUILDTIMEFILENAME := $(shell date -u '+%Y%m%d-%H%M%SZ')
IMAGENAME := sneak/$(APPNAME)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildtime=$(BUILDTIME)
GOLDFLAGS += -X main.Builduser=$(BUILDUSER)@$(BUILDHOST)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)
GOLDFLAGS += -X main.Appname=$(APPNAME)

UNAME_S := $(shell uname -s)

# osx can't statically link apparently?!
ifeq ($(UNAME_S),Darwin)
	GOFLAGS := -ldflags "$(GOLDFLAGS)"
endif

ifneq ($(UNAME_S),Darwin)
	GOFLAGS = -ldflags "-linkmode external -extldflags -static $(GOLDFLAGS)"
endif

default: run

run: build
	DEBUG=1 PORT=1111 ./$(APPNAME)

build: ./$(APPNAME)

./$(APPNAME): *.go models/*.go cmd/*/*.go
	cd ./cmd/$(APPNAME) && go build -o ./$(APPNAME) $(GOFLAGS) . && mv ./$(APPNAME) ../..

clean:
	rm ./$(APPNAME)

fmt:
	go fmt *.go

lint:
	which revive || go get -u github.com/mgechev/revive
	revive -formatter friendly -config .revive.toml -exclude=./vendor/... ./... || exit 1


test: build-docker-image

dist: build-docker-image
	-mkdir -p ./output
	docker run --rm --entrypoint cat $(IMAGENAME) /bin/$(APPNAME) > output/$(APPNAME)
	docker save $(IMAGENAME) | bzip2 > output/$(BUILDTIMEFILENAME).$(APPNAME).tbz2

hub: upload-docker-image

build-docker-image:
	docker build -t $(IMAGENAME) .

upload-docker-image: build-docker-image
	docker tag $(IMAGENAME) $(IMAGENAME):$(BUILDTIMETAG)
	docker push $(IMAGENAME):$(BUILDTIMETAG)

ci-upload-images:
	docker tag $(IMAGENAME) $(IMAGENAME):$(CIRCLE_SHA1)
	docker tag $(IMAGENAME) $(IMAGENAME):$(CIRCLE_BRANCH)
	docker push $(IMAGENAME):$(CIRCLE_SHA1)
	docker push $(IMAGENAME):$(CIRCLE_BRANCH)
