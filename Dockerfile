FROM golang:1.13 as builder

WORKDIR /go/src/github.com/sneak/merp
COPY . .

RUN go get -v && make build

WORKDIR /go
RUN tar cvfz go-src.tgz src && du -sh *

FROM alpine

COPY --from=builder /go/src/github.com/sneak/merp/merp /bin/merp

# put the source in there too for safekeeping
COPY --from=builder /go/go-src.tgz /usr/local/src/go-src.tgz
#COPY --from=builder /go/src /usr/local/src/go

CMD /bin/merp

# FIXME add testing
