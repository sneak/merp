package merp

import "os"
import "time"
import "github.com/astaxie/beego/orm"
import "github.com/sneak/merp/models"
import "github.com/rs/zerolog/log"
import _ "github.com/lib/pq" //revive:disable-line

// GetDB returns an orm.Ormer so that the API server can talk to the
// database
func GetDB() orm.Ormer {
	if os.Getenv("DEBUG") != "" {
		orm.Debug = true
	}

	o := connectDB()
	syncDB(o)

	return o
}

// ConnectToDb - Initializes the ORM and Connection to the postgres DB
func connectDB() orm.Ormer {
	orm.DefaultTimeLoc = time.UTC
	dbURL := os.Getenv("POSTGRES_DB_URL")
	orm.RegisterDriver("postgres", orm.DRPostgres)
	orm.RegisterDataBase("default", "postgres", dbURL)
	orm.SetMaxIdleConns("default", 1)
	orm.SetMaxOpenConns("default", 5)
	orm.RegisterModel(new(models.Merp))
	o := orm.NewOrm()
	o.Using("default")
	return o
}

// SyncDB() is responsible for creating the schema in the database
func syncDB(o orm.Ormer) {
	// Database alias.
	name := "default"
	// Drop table and re-create.
	force := false
	// Print log.
	verbose := true
	// Error.
	err := orm.RunSyncdb(name, force, verbose)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
}
