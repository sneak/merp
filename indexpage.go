package merp

//3456789112345676892123456789312345678941234567895123456789612345678971234567898

const basePage = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
		{{.Head}}
	</head>
	<body>
		{{.Body}}
	</body>
</html>`
